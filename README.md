# IntroTA
# Last update: 30 / 10 / 2020

Files for the "Introduction to Test Automation" training:
- exercises: contains all the exercises (1, 2 & 3)
- contactForm.feature: feature file used in the exercises
- contactForm.js: .js file used in the exercises

TIPS:
- for Cypress installation under Windows, use the PowerShell (and make sure you have admin rights)
- for exercise 1 use https://www.npmjs.com/package/cypress-cucumber-preprocessor to copy-paste code lines