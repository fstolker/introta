import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";

Given('I am at the {string} website', (url) => {
  cy.visit(url)
  cy.url().should('eq', url)
})

When('I navigate to the {string} page', (pageName) => {
  cy.get("#navbar-collapse > ul > li > a[href='#"+pageName+"']").click()
})

Then('the {string} page is loaded', (pageName) => {
  cy.get("#"+pageName).contains(pageName.charAt(0).toUpperCase() + pageName.slice(1))
})

Given('I am at the {string} page', (pageName) => {
  cy.get("#"+pageName).contains(pageName.charAt(0).toUpperCase() + pageName.slice(1))
})

When('I fill out the contact form and submit it', (dataTable) => {
  dataTable.hashes().forEach(row => {
    cy.get("#name").type(row["Name"])
    cy.get("#tel").type(row["Phone Number"])
    cy.get("#mail").type(row["Email"])
    cy.get("#subject").type(row["Subject"])
    cy.get("#submit-message").click()    
  })

})

Then('I get the message {string}', (message) => {
  cy.get("#msg").contains(message)
})
