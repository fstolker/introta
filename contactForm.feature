Feature: Aurum Tempus Contact Form
 
  As an AT visitor
  I want to fill out the contact form
  So that I can ask a question and expect an answer
  
  Scenario: Happy Flow: Go to AT web site, fill out contact form, and submit it
    Given I am at the "https://www.aurumtempus.nl/" website
    When I navigate to the "contact" page
    Then the "contact" page is loaded

    Given I am at the "contact" page
    When I fill out the contact form and submit it
    | Name    | Phone Number | Email           | Subject          |
    | Cursist | 1234567890   | mail@cursist.nl | Request for info |
    Then I get the message "Uw bericht is verzonden. Dank u!"